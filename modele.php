<?php

// Ce fichier rassemble tout ce qui est lié aux données....
// Configuration
// Serveur LDAP (pour vérification des login)
$ldap['host'] = 'ldap://ldap.lisn.upsaclay.fr';
$ldap['port'] = '389';
$ldap['basedn'] = 'dc=lisn,dc=fr';
$dn_anonyme = "ou=actifs,ou=people," . $ldap['basedn'];

// Le lien qui doit être envoyé aux visiteurs 
$hote="services";
$domaine="lisn.upsaclay.fr";

$url_form = "https://$hote.$domaine/visites/info.php";
$url_ok = "https://$hote.$domaine/visites/present/ok.php";
$url_present = "https://$hote.$domaine/visites/present/index.html";


// Dossier à créer 
$dossier_tmp = "/var/upload/visites/";


// Le fichier rdf qui contient tout :
//$debut_xml = "<rdf:RDF  xmlns=\"http://www.w3.org/2002/12/cal#\">\n";

$sherlock = "visites@lisn.upsaclay.fr";
$debut_xml = "<?xml version='1.0' standalone='yes'?>\n";

/* Utilisé dans le formulaire de déclaration de présence en dehors des horaires */
$vigile = "visites@lisn.upsaclay.fr";

// Entete du message
$entete = "From: $sherlock \r\n";
$entete .= 'Content-type: text; charset=utf8' . "\r\n";
$entete .= 'X-Mailer: PHP/' .  phpversion();

$fic_log = $dossier_tmp . "log.csv";
$fic_vigile = $dossier_tmp . "vigile.csv";


// Définition d'une visite

class Visite {

    public $id;  // Id de la visite
    public $limsien; // Login de la personne du laboratoire accueillant
    public $motif; // Motif de la visite
    public $date1; // Date du début de la visite
    public $date2; // Date de la fin de la visite
    public $cn;     // Comminame

    function __construct() {
        $maintenant = time();
        $this->id = "v$maintenant"; // idtemporaire
        $this->limsien = "";        // login de l'invitant
        $this->cn = "";             // cn (LDAP) Prénom Nom
        $this->motif = "";          // Motif de la réunion
        $this->date1 = "";          // Date de début
        $this->date2 = "";          // Date de fin
    }

    function recup() {
        if (isset($_POST)) {
            if (isset($_POST['login'])) {
                $this->limsien = $_POST['login'];
                $this->id = date("z") . substr($this->limsien, 0, 1) . date("Gi");
            }
            if (isset($_POST['pourquoi']))
                $this->motif = $_POST['pourquoi'];
            if (isset($_POST['date1']))
                $this->date1 = $_POST['date1'];
            if (isset($_POST['date2']))
                $this->date2 = $_POST['date2'];
        }

        $this->verifLdap();
    }

// Affiche l'évenement dans une pae html
    function affiche() {
        $urlcontact = "";
        echo "\n<article>\n";
        echo "<header><h2>" . $this->motif . "</h2></header>";
        echo "<p>Dates : " . $this->date1 . " - " . $this->date2 . "</p>";
        echo "<p>Contact : " . $this->cn . "</p>";

        echo "\n</article>\n";
    }

    function cree_xml() {
        global $dossier_tmp, $debut_xml;

        $contenu = "<Vevent>\n";
        $contenu .= " <uid>" . $this->id . "@" . $this->limsien . "</uid>\n";
        $contenu .= " <dtstart>" . $this->date1 . "</dtstart>\n";
        $contenu .= " <dtend>" . $this->date2 . "</dtend>\n";
        $contenu .= " <summary>" . $this->motif . "</summary>\n";
        $contenu .= " <Location>LISN</Location>\n";
        $contenu .= " <Organizer>" . $this->cn . "</Organizer>\n";
        $contenu .= "</Vevent>\n\n";

        $fich_xml = $dossier_tmp . "/" . $this->id . ".xml";
        if (($handler = fopen($fich_xml, "w")) !== FALSE) {
            fprintf($handler, $debut_xml);
            fprintf($handler, $contenu);
        } else {
            echo "<p>PB écriture $fich_xml</p>";
        }
    }

    function lit_xml() {
        global $dossier_tmp;
        $fich_xml = $dossier_tmp . $this->id . ".xml";

        libxml_use_internal_errors(true);
        if (file_exists($fich_xml))
            $reunion = simplexml_load_file($fich_xml);
        else
            die("Erreur ouverture $fich_xml");

        if ($reunion === false) {
            echo "Failed loading XML\n";
            foreach (libxml_get_errors() as $error) {
                echo "\t", $error->message;
            }
        } else {
            $this->date1 = $reunion->dtstart;
            $this->date2 = $reunion->dtend;
            $this->motif = $reunion->summary;
            $tmp = explode("@", $reunion->uid);
            $this->limsien = $tmp[1];
            $this->cn = $reunion->Organizer;
        }
    }

// On va verifier les informations LDAP
    function verifLdap() {
        global $ldap, $dn_anonyme;

        $ds = @ldap_connect($ldap['host'], $ldap['port']);

        if (empty($ds))
            exit("Pas de connexion au serveur LDAP");

        ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);

        $filtre = "(uid=" . $this->limsien . ")";

        $read = ldap_search($ds, $dn_anonyme, $filtre) or exit("Erreur LDAP SEARCH : $ds");
        $info = ldap_get_entries($ds, $read);
      
        if ($info['count'] > 0) {
            $this->cn = ($info[0]['cn'][0]);
        }

        ldap_close($ds);
    }

    function affiche_url() {
        global $url_form, $sherlock, $entete;
        $url = $url_form . "?code=" . $this->id;

// Envoie un email pour vérifier

        $dest = $this->limsien . "@lisn.fr";

        $sujet = "Pour les visiteurs du $this->date1" ;
        $sujet2 = "[Visite " . $this->id . "] Suivi ";

        $message = <<<FIN
J'organise une réunion : $this->motif,  du $this->date1 au $this->date2.
    
Merci de remplir le formulaire suivant :
$url
        
---------

FIN;

/*
        $message = <<<FIN
Bonjour $this->cn,

* Information à communiquer aux visiteurs extérieurs  :             
---------
J'organise une réunion : "$this->motif",  du $this->date1 au $this->date2.
    
Merci de remplir le formulaire suivant :
$url
        
Accès 
Belvédère (507/508/512) : http://www.limsi.fr/Pratique/acces/    
Plaine (650/660) : https://www.lri.fr/info.pratiques.php
---------

FIN;
*/
        $bonus = "       
 Bonjour $this->cn,
     
   - Un email sera envoyé à $sherlock à chaque inscription.
   - Vous pouvez vérifier que les personnes sont inscrites via : $url&action=liste
   - En cas d'usurpation d'identité,  merci d'alerter sami@lisn.fr
  
";

        echo "<p>Deux messages ont été envoyés à $this->cn :";
        echo "<h3>$sujet</h3>";
	echo "<pre>$message</pre>";
        mail($dest, $sujet, $message, $entete);

        echo "<h3>$sujet2</h3>";

	echo "<pre>$bonus</pre>";
        mail($dest, $sujet2, $bonus, $entete);
    }

    function signature() {
        global $url_form;
        $url = $url_form . "?action=liste&code=" . $this->id;
        $fin = "";

        $fin = "\n\nDate de la visite : " . $this->date1 . " - " . $this->date2;
        $fin .= "\nMotif : " . $this->motif;
        $fin .= "\nContact : " . $this->cn . " (" . $this->limsien . "@lisn.fr)";
        $fin .= "\n\nVisiteurs : $url\n";

        return $fin;
    }

    // On stocke les informations dans un fichier csv
    function lit_csv() {
        global $fic_log;

        if (($handle = fopen($fic_log, "r")) !== FALSE) {
            echo "<h2>Liste des visiteurs autorisés</h2>";
            echo "<ol>";
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($data[1] == $this->id) {
                    echo "<li>" . $data[0] . " : " . $data[2] . " " . $data[3];
                    echo "</li>";
                }
            }
            echo "</ol>";
        } else {
            echo "Problème de lecture de $fic_log";
        }
    }

}

// Définition d'un visiteur

class Visiteur {

    public $id; // Identifiant
    public $nom; // Nom du visiteur
    public $prenom; // Nom du visiteur
    public $email; // email de la personne
    public $organisme; // laboratoire de la personne
    public $fonction; // fonction
    public $date_naissance; // Date de naissance
    public $nationalite; // Nationalité
    public $lieu_naissance; // Lieu de naissance

    function __construct() {
        $nsp = "On ne sait pas";
        $this->id = ""; // On met l'ID à zero
        $this->nom = "";
        $this->prenom = $nsp;
        $this->nationalite = $nsp;
        $this->organisme = $nsp;
        $this->fonction = $nsp;
        $this->lieu_naissance = "un endroit sur la Terre";
        $this->date_naissance = "14/07/1789";
    }

    // Récupère les données du questionnaire
    function recup() {

        if (isset($_POST['nom']))
            $this->nom = $_POST['nom'];
        if (isset($_POST['prenom']))
            $this->prenom = $_POST['prenom'];
        if ((isset($_POST['date'])) AND ($_POST['date'] != ""))
            $this->date_naissance = $_POST['date'];
        if ((isset($_POST['lieu_naissance'])) AND ($_POST['lieu_naissance'] != ""))
            $this->lieu_naissance = $_POST['lieu_naissance'];
        if ((isset($_POST['nationalite'])) AND ($_POST['nationalite'] != ""))
            $this->nationalite = $_POST['nationalite'];
        if ((isset($_POST['organisme'])) AND ($_POST['organisme'] != ""))
            $this->organisme = $_POST['organisme'];
        if ((isset($_POST['fonction'])) AND ($_POST['fonction'] != ""))
            $this->fonction = $_POST['fonction'];
    }

    function MailZRR($id) {
        $sujet = "[Visite $id] " . $this->nom;

        $message = "Nom : " . $this->nom;

        if ($this->prenom != "") {
            $sujet .= " (" . $this->prenom . ")";
            $message .= "\nPrénom : " . $this->prenom;
        }

        $message .= "\nNé(e) le " . $this->date_naissance . " à " . $this->lieu_naissance;
        
        $message .= "\nNationalité : " . $this->nationalite; 

        $message .= "\nFonction : " . $this->fonction;

        $message .= "\nOrganisme : " . $this->organisme;


        $tableau = array(
            "sujet" => $sujet,
            "message" => $message
        );

        return $tableau;
    }

    // On ajoute une ligne dans le fichier de log;
    function AjoutLog($id) {
        global $fic_log, $sherlock;


        $lignecsv = date("Y-m-d H:i:s");
        $lignecsv .= ",$id";
        $lignecsv .= "," . $this->nom . "," . $this->prenom . "," . $sherlock . "\n";

        if (($handler = fopen($fic_log, "a")) !== FALSE)
            fprintf($handler, $lignecsv);
        else {
            echo "Impossible d'ouvrir $fic_log";
        }
    }

    function Merci() {
        echo "<p>Merci ";
        echo $this->prenom . " " . $this->nom . " !</p>";
        echo "<p>Votre venue au LISN est enregistr&eacute;e.</p>";
    }

}

class Presence {

    public $id;  // Id de la visite
    public $limsien; // Login de la personne du labo présente au labo
    public $cn;     // Commun name
    public $h_matin; // Présence le matin
    public $h_soir; // Présence le soir

    function __construct() {
        $maintenant = time();
        $this->id = "v$maintenant"; // idtemporaire
        $this->limsien = "";        // login de l'invitant
        $this->cn = "";             // cn (LDAP) Prénom Nom
        $this->h_matin = "08:30";          // Date de début
        $this->h_soir = "18:30";          // Date de fin
    }

    function recup() {
        if (isset($_POST)) {
            if (isset($_POST['loginp'])) {
                $this->limsien = $_POST['loginp'];
                $this->id = date("z") . $this->limsien;
            }

            if ((isset($_POST[matin])) AND ($_POST[matin] != ""))
                $this->h_matin = $_POST[matin];
            if ((isset($_POST[soir])) AND ($_POST[soir] != ""))
                $this->h_soir = $_POST[soir];
        }


        $this->verifLdap();
    }

// Affiche l'évenement dans une page html
    function affiche() {
        $urlcontact = "";
        echo "\n<article>\n";
        echo "<header><h2>Présence " . $this->cn . "</h2></header>";

        echo "\n</article>\n";
    }

    function cree_xml() {
        global $dossier_tmp, $debut_xml;

        $contenu = "<Vevent>\n";
        $contenu .= " <uid>" . $this->id . "</uid>\n";
        $contenu .= " <summary> Presence </summary>\n";
        $contenu .= " <Location>LISN</Location>\n";
        $contenu .= " <Organizer>" . $this->cn . "</Organizer>\n";
        $contenu .= " <dtstart>" . $this->h_matin . "</dtstart>\n";
        $contenu .= " <dtend>" . $this->h_soir . "</dtend>\n";
        $contenu .= "</Vevent>\n\n";

        $fich_xml = $dossier_tmp . "/" . $this->id . ".xml";
        if (($handler = fopen($fich_xml, "w")) !== FALSE) {
            fprintf($handler, $debut_xml);
            fprintf($handler, $contenu);
        } else {
            echo "<p>PB écriture $fich_xml</p>";
        }
    }

    function lit_xml() {
        global $dossier_tmp;
        $fich_xml = $dossier_tmp . $this->id . ".xml";

        libxml_use_internal_errors(true);
        if (file_exists($fich_xml))
            $reunion = simplexml_load_file($fich_xml);
        else
            die("Erreur ouverture $fich_xml");

        if ($reunion === false) {
            echo "Failed loading XML\n";
            foreach (libxml_get_errors() as $error) {
                echo "\t", $error->message;
            }
        } else {

            $tmp = explode("@", $reunion->uid);
            $this->limsien = $reunion->uid;
            $this->cn = $reunion->Organizer;
            $this->h_matin = $reunion->dtstart;
            $this->h_soir = $reunion->dtend;
        }
    }

// On va verifier les informations LDAP
    function verifLdap() {
        global $ldap, $dn_anonyme;

        $ds = @ldap_connect($ldap[host], $ldap[port]);

        if (empty($ds))
            exit("Pas de connexion au serveur LDAP");

        ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);

        $filtre = "(uid=" . $this->limsien . ")";

        $read = ldap_search($ds, $dn_anonyme, $filtre) or exit("Erreur LDAP SEARCH : $ds");
        $info = ldap_get_entries($ds, $read);

        if ($info[count] > 0) {
            $this->cn = ($info[0][cn][0]);
        }

        ldap_close($ds);
    }

    function affiche_url() {
        global $url_ok, $sherlock, $entete, $url_present;
        $url = $url_ok . "?profil=" . $this->id;
        $out = FALSE;

// Envoie un email pour vérifier

        $dest = $this->limsien . "@lisn.fr";


        $sujet = "Présence au LISN ";

        if ($this->h_matin != "08:30") {
            $sujet .= " à " . $this->h_matin;
            $out = TRUE;
        }

        if ($this->h_soir != "18:30") {
            $sujet .= " jusqu'à " . $this->h_soir;
            $out = TRUE;
        }
        $message = <<<FIN
Bonjour $this->cn,
        
Pour confirmer que tu es au LISN aujourd'hui en dehors des heures d'ouverture, 
merci de cliquer sur ce lien :
    $url

Cordialement,
                
La direction
                
------------------------
Message généré depuis :  $url_present
Si tu n'as pas utilisé ce formulaire, merci de contacter sami@lisn.fr 
Le lien peut être réutilisé. Chaque clic envoie un email à presence@lisn.fr
               
FIN;




        $bonus = "       
 Bonjour $this->cn,
     
   - Un email sera envoyé à $sherlock à chaque présence.
   - Si tu n'as pas créé de formulaire, merci d'alerter sami@lisn.fr
  
";

        if ($out) {
        echo "<p>Un message a été envoyé à $this->cn :";
        echo "<br/><b>$sujet</b>";
        mail($dest, $sujet, $message, $entete);
        } else {
            echo "<p>Le LISN est ouvert aux horaires indiqués. Pas de message envoyé</p>";
        }
    }


    function MailVigipirate() {
        $sujet = $this->cn . ",";

        $message = "Nom : " . $this->cn;

        if ($this->limsien != "") {
            $message .= "\nRéférence : " . $this->limsien;
        }

        if ($this->h_matin != "08:30") {
            $sujet .= " " . $this->h_matin . " ";
            $message .= "\nMatin : " . $this->h_matin;
        }

        if ($this->h_soir != "18:30") {
            $sujet .= " " . $this->h_soir;
            $message .= "\nSoir : " . $this->h_soir;
        }


        if ($this->motif != "") {
            $message .= "\nMotif : " . $this->motif;
        }

        $tableau = array(
            "sujet" => $sujet,
            "message" => $message
        );

        return $tableau;
    }

    function AjoutLog() {
        global $fic_vigile, $vigile;


        $lignecsv = date("Y-m-d H:i:s");
        $lignecsv .= ",$this->cn";
        $lignecsv .= "," . $this->h_matin . "," . $this->h_soir . "," . $vigile . "\n";

        if (($handler = fopen($fic_vigile, "a")) !== FALSE)
            fprintf($handler, $lignecsv);
        else {
            echo "Impossible d'ouvrir $fic_vigile";
        }
    }
}

?>
