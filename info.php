<!DOCTYPE html>
<html>
    <head>
        <title>Visite au laboratoire</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="accueil.css" media="screen"/>
    </head>
    <body>
        <h1>Visite du XXXX</h1>
        <?php
        include_once 'modele.php';


	/*
	 * On arrive avec une adresse du style : 
	 * https://services.lisn.upsaclay.fr/visites/info.php?code=35z1052
	 */ 
        if (isset($_GET['code'])) {
            $reunion = new Visite;
            $reunion->id = $_GET['code'];

	    /* on lit les infos de la réunion et on les affiche 
	     * <p>Dates : 2024-02-07 - 2024-02-28</p>
	     * <p>Contact : Elisabeth Piotelat</p>
            */
            $reunion->lit_xml();
            $reunion->affiche();
            if (isset($_GET['action'])) {
		    /* Si c'est l'organisateur qui arrive avec info.php?code=35z1052&action=liste 
			    * on liste les visiteurs inscrits */
                $reunion->lit_csv();
            } else {
		    /* Sinon, on affiche le formulaire */
                echo "<form action=\"info.php\" method=\"post\">";
                echo "<input type=\"hidden\" name=\"id\" value=\"" . $reunion->id . "\">";
                include ('visiteur.html');
            }
        }

	/* L'organisateur vient de se connecter */
        if (isset($_POST['login'])) {
            $visite_du_jour = new Visite;
            $visite_du_jour->recup();
            if ($visite_du_jour->cn == "") {
                echo "<p>Aucun membre du XXXX n'a été trouvé ($visite_du_jour->limsien) ";
                echo "<a href=\"index.html\">Retour</a></p>";
                exit();
            } else {
                $visite_du_jour->affiche();
                $visite_du_jour->cree_xml();
                $visite_du_jour->affiche_url();
            }
        }

	/* Un visiteur vient de valider le formulaire */
        if (isset($_POST['id'])) {
            $reunion = new Visite;
            $reunion->id = $_POST['id'];
            $reunion->lit_xml();


            $fin = $reunion->signature();

            $visiteur = new Visiteur;

            $visiteur->recup();

            $mail = $visiteur->MailZRR($reunion->id);

            $mail['message'] .= $fin;

            mail($sherlock, $mail['sujet'], $mail['message'], $entete);

            $visiteur->AjoutLog($reunion->id);

            $visiteur->merci();
            $reunion->affiche();
        }


        ?>

        <footer>
            <a href="cnil.html">Mentions légales</a>
        </footer>
    </body>
</html>
